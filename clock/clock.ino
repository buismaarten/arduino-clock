uint16_t counter = 0;
uint32_t current_millis = 0;
uint32_t previous_millis = 0;

const uint8_t DELAY = 5;
const uint16_t INTERVAL = 1000;
const uint8_t DIGIT_PINS[] = { 9, 10, 11, 12 };
const uint8_t SEGMENT_PINS[] = { 2, 3, 4, 5, 6, 7, 8 };
const byte DIGIT_CODES[] = { B00111111, B00000110, B01011011, B01001111, B01100110, B01101101, B01111101, B00000111, B01111111, B01101111 };

const uint8_t SEGMENT_COUNT = sizeof(SEGMENT_PINS) / sizeof(SEGMENT_PINS[0]);
const uint8_t DIGIT_COUNT = sizeof(DIGIT_PINS) / sizeof(DIGIT_PINS[0]);
const uint16_t COUNTER_MAX_VALUE = pow(10, DIGIT_COUNT) - 1;

void setup() {
  for (uint8_t pin : DIGIT_PINS) {
    pinMode(pin, OUTPUT);
  }

  for (uint8_t pin : SEGMENT_PINS) {
    pinMode(pin, OUTPUT);
  }
}

void loop() {
  current_millis = millis();

  if (current_millis - previous_millis >= INTERVAL) {
    previous_millis = current_millis;

    counter++;
    counter = constrain(counter, 0, COUNTER_MAX_VALUE);
  }

  display_number(counter);
}

void display_number(uint16_t number) {
  for (uint8_t i = 0; i < DIGIT_COUNT; i++) {
    select_digit_number((uint32_t) (number / pow(10, i)) % 10);
    select_digit_index((DIGIT_COUNT - i) - 1);
    delay(DELAY);
  }
}

void select_digit_number(uint8_t number) {
  for (uint8_t segment_index = 0; segment_index < SEGMENT_COUNT; segment_index++) {
    digitalWrite(SEGMENT_PINS[segment_index], LOW);

    if (DIGIT_CODES[number] & (1 << segment_index)) {
      digitalWrite(SEGMENT_PINS[segment_index], HIGH);
    }
  }
}

void select_digit_index(uint8_t index) {
  for (uint8_t pin : DIGIT_PINS) {
    digitalWrite(pin, HIGH);
  }

  digitalWrite(DIGIT_PINS[index], LOW);
}
